# 3.3.0

3.3.0 is a relatively major release with many bugfixes and a number of features added based on consultation
with developers. One major change is that versions are now parsed as MAJOR.MINOR.PATCH-BUILD (with build
number being optional, assumed to be zero if not present). This has proven to be necessary in the NFS
setting for a number of separate reasons to handle dependencies properly. Note the following behavior:

* `require module` - loads the latest numeric version of a given module.
* `require module,x.x.x` - loads the specific numeric version of a module, with the highest build number
* `require module,x.x.x-x` - loads a specific build number of the specified version of a module

A second major change (mostly via bugfixes) is that the local install command, `make cellinstall` which installs the module in a local directory now works properly, which allows developers and integrators to simply mount the NFS E3 build and work with that instead of needing to install E3 locally.

## New Features

* Removed all EPICS 3.* and VxWorks code, as these are not to be supported at ESS.
* Consistent with the philosophy of not requiring module version pinning, if one specifies a dependent
  module with e.g. `REQUIRED += asyn` then the latest version of asyn will be used. No version need
  to be specified.
* Installed modules will be installed together with a metadata file containing information about the
  module and wrapper, including the git status of the wrapper/module, the latest tag/commit hash, and
  the git url for the wrapper.
* Build numbers are now supported properly
* `iocsh.bash` will load `env.sh` at startup (which can be specified), which allows `IOCNAME` to be
  specified at startup
* Added the ability to install header files while preserving directory structure instead of flattening
  all header files into a single module/version/includes directory.
* Using `EPICS_MODULE_TAG` to detect if we are using the local source code vs. git submodule. Note that
  the old -loc is still supported, but will be deprecated in a future release.
* Added a `prebuild` target that runs before build so module developers can run specific code before the build process.
* A module developer can now install dbd files separate from the module dbd file by using `DBD_INSTALLS += file.dbd`.
  
## Bugfixes

* Ensures that lowercase module names are enforced consistently
* Vendor libraries are only installed at install time, not at build time
* Vendor libraries are uninstalled when `make uninstall` is run
* `make debug` can now be run before running `make build`
* Patches that involve renaming files can be used
* When using the "cellinstall" mode to install modules for local testing, sudo has been removed.
* When using "cellinstall", sequencer is loaded correctly
* When using "cellinstall", module include files are located correctly
* `iocsh.bash` now supports multiple directories being specified with the -l (local) flag as a source of loading modules
* Fixed an error where mounting the NFS E3 in a location other than `/epics` would result in failed builds
* Assorted minor typos in scripts
* Re-added functionality to have `iocsh.bash` automagically source `setE3Env.bash`.

# 3.2.0

Require 3.2.0 is the first version of require used during our initial return to the NFS implementation
of E3. It maintains some of the work done there (modules are lowercased for consistency), but mostly
reverts back to a similar version to require 3.1.2. One major difference is that module pinning is not
"required" (pun only semi-intended). That is, a startup script can simply include a statement of the
form `require module` instead of `require module,version` as was the case in 3.1.2. An IOC developer can
of course pin a specific module version. If they do not, then the highest numeric version will be chosen.

## New Features

* Added -dg, -dv options to run gdb and valgrind using `iocsh.bash`
* If `IOCNAME` is defined, then it is used in the PV names set by require instead of `REQMOD:$(hostname)-$(pid)`.
* Reverted changes from conda require to return to NFS
* Moved tools files from e3-require to require submodule

## Bugfixes

* Fixed issue where a second user running `iocsh.bash` on a machine would be unable to create the temporary
  startup script
